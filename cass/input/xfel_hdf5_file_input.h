// Copyright (C) 2017 Lutz Foucar

/**
 * @file xfel_hdf5_file_input.h contains a xfel hdf5 file reader class
 *
 * @author Lutz Foucar
 */

#ifndef _XFELHDF5FILEINPUT_H_
#define _XFELHDF5FILEINPUT_H_

#include <string>

#include "input_base.h"
#include "cass.h"
#include "ringbuffer.hpp"
#include "cass_event.h"
#include "file_reader.h"

namespace cass
{
/** XFEL HDF5 File Input for cass
 *
 * @cassttng XFELHDF5FileInput/{MaxPulseId}\n
 *           The maximum pulse id that will be used to iterate through. Default is 65.
 * @cassttng XFELHDF5FileInput/{PreCacheData}\n
 *           Flag that allows precaching the whole files by reading the contents to memory.
 *           Need to ensure that you've got enough memory available first otherwise the
 *           program will crash. Default is true.
 * @cassttng XFELHDF5FileInput/AGIPD/{DataCASSID}\n
 *           ID that the agipd data will have in the cassevent. Default is 30
 * @cassttng XFELHDF5FileInput/AGIPD/{MaskCASSID}\n
 *           ID that the agipd mask will have in the cassevent. Default is 31
 * @cassttng XFELHDF5FileInput/AGIPD/{GainCASSID}\n
 *           ID that the agipd gain information will have in the cassevent. Default is 32
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{Id}\n
 *           ID of the tile. Default is -1 which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{HDF5FileName}\n
 *           HDF5 file that contains the data of the tile.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{DataHDF5Key}\n
 *           Key to the data of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{MaskHDF5Key}\n
 *           Key to the mask of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{GainHDF5Key}\n
 *           Key to the gain data of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{TrainIdHDF5Key}\n
 *           Key to the train Id of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{PulseIdHDF5Key}\n
 *           Key to the pulse Id of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 * @cassttng XFELHDF5FileInput/AGIPD/Tiles/\%index\%/{CellIdHDF5Key}\n
 *           Key to the cell Id of the tile in the HDF5 file.
 *           Default is Invalid which will skip this entry
 *
 * @author Lutz Foucar
 */
class XFELHDF5FileInput :  public InputBase
{
public:
  /** create instance of this
   *
   * @param filelistname name of the file containing all files that should be
   *                     processed
   * @param ringbuffer reference to the ringbuffer containing the CASSEvents
   * @param ratemeter reference to the ratemeter to measure the rate of the input
   * @param loadmeter reference to the ratemeter to measure the load of the input
   * @param quitwhendone flag that tells this class that it should quit the
   *                     Program when its done reading all events
   * @param parent The parent QT Object of this class
   */
  static void instance(std::string filelistname,
                       RingBuffer<CASSEvent>&,
                       Ratemeter &ratemeter,
                       Ratemeter &loadmeter,
                       bool quitwhendone,
                       QObject *parent=0);

  /** function with the main loop */
  void runthis();

  /** load the parameters used for the multifile input */
  void load();

  /** retrieve the averaged progress state of all file processors
   *
   * @return the processing progress
   */
  double progress() {return static_cast<double>(_counter) /
                            static_cast<double>(_totalevents);}

  /** retrieve the number of processed events
   *
   * @return number of processed events
   */
  uint64_t eventcounter() {return _counter;}

  /** retrieve the number of skipped processed events
   *
   * @return number of processed events
   */
  uint64_t skippedeventcounter() {return _scounter;}

public:
  /** parameters needed to retrieve the Acqiris data */
  struct AcqirisParams
  {
    /** name of the waveform datafield */
    std::string DataKey;
    /** name of the field that contains the horpos value */
    std::string HorposKey;
    /** name of the field that contains the vertical offset value */
    std::string VertOffsetKey;
    /** name of the field that contains the gain value */
    std::string GainKey;
    /** name of the field that contains the sample interval */
    std::string SampleIntervalKey;
    /** the instrument within the CASSEvent that should be used */
    uint32_t Instrument;
    /** the channel with the instrument within the CASSEvent that the data should
     *  loaded to
     */
    size_t ChannelNumber;
  };

  /** parameters needed to retrieve the pixeldetector data */
  struct PixeldetectorParams
  {
    /** name of the waveform datafield */
    std::string DataKey;

    /** the id of the pixeldetector that it should have in the CASSEvent */
    int CASSID;
  };

private:
  /** constructor
   *
   * @param filelistname name of the file containing all files that should be
   *                     processed
   * @param ringbuffer reference to the ringbuffer containing the CASSEvents
   * @param ratemeter reference to the ratemeter to measure the rate of the input
   * @param loadmeter reference to the ratemeter to measure the load of the input
   * @param quitwhendone flag that tells this class that it should quit the
   *                     Program when its done reading all events
   * @param parent The parent QT Object of this class
   */
  XFELHDF5FileInput(std::string filelistname,
                    RingBuffer<CASSEvent>&,
                    Ratemeter &ratemeter,
                    Ratemeter &loadmeter,
                    bool quitwhendone,
                    QObject *parent=0);

  /** flag to tell the thread to quit when its done with all files */
  bool _quitWhenDone;

  /** name of the file containing all files that we need to process */
  std::string _filelistname;

  /** the counter for all events */
  uint64_t _counter;

  /** the counter for all events */
  uint64_t _scounter;

  /** the total # of events to be handled */
  uint64_t _totalevents;
};

}//end namespace cass

#endif
