// Copyright (C) 2012 Lutz Foucar

/**
 * @file data_generator.cpp file contains base class for all data generators.
 *
 * @author Lutz Foucar
 */

#include <stdexcept>

#include "data_generator.h"


using namespace cass;
using namespace std;

DataGenerator::~DataGenerator()
{

}

void DataGenerator::fill(CASSEvent &)
{

}

void DataGenerator::load()
{

}
