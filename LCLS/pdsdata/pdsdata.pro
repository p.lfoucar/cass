# Copyright (C) 2013 Lutz Foucar

TEMPLATE       = subdirs
SUBDIRS        = acqiris \
                 app \
                 bld \
                 camera \
                 compress \ # switched from boost to stl
                 control \
                 cspad \
                 cspad2x2 \
                 encoder \
                 epics \
                 evr \
                 fccd \
                 ipimb \
                 lusi \
                 opal1k \
                 pnCCD \
                 princeton \
                 psddl \
                 pulnix \
                 xtc
