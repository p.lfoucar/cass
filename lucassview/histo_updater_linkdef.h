/**
 * @file histo_updater_linkdef.h definition for root cint
 *
 * @author Lutz Foucar
 */

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class HistogramUpdater+;
#pragma link C++ global gCASSClient;

#endif
